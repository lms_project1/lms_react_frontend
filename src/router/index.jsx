import { Navigate, Route, Routes } from "react-router-dom";
import { MainLayout } from "../Layouts/MainLayout";
import { AuthRoutes } from "modules/Auth/routes";
import { observer } from "mobx-react-lite";
import { authStore } from "store/auth.store";
import { UsersLayouts } from "../LayoutsUser/UserLayout";
import { UserCourseRoutes } from "modules/User/Course/routes";
import { UserExamRoutes } from "modules/User/Exam/routes";
import { UserProfileRoutes } from "modules/User/Profile/routes";
import { UsersRoutes } from "modules/Admin/Users/routes";
import { CourseRoutes } from "modules/Admin/Course/routes";
import { AdminLayouts } from "../LayoutsAdminProfile/AdminLayouts";
import { AdminRoutes } from "../modules/Admin/Profile/routes";
import { InformationRoutes } from "../modules/Admin/Profile/routes/Information";
import { DeviceRoutes } from "../modules/Admin/Profile/routes/Device";
import { Profile } from "../modules/Admin/Profile/routes/Profile";
import { toJS } from "mobx";

export const Router = observer(() => {

  const isAuth = authStore.isAuth

  const dataType = authStore.userData?.user_data?.role

  if(!isAuth) {
    return <Routes>
    <Route path="/" element={<AuthRoutes/>} />
    <Route path="/auth/*" element={<AuthRoutes/>} />
    <Route path="/*" element={<Navigate to="/auth" />}/>
  </Routes>
  }

  if(dataType === 'admin'){
    return <Routes>
    <Route path="" element={<MainLayout />}>
      <Route path="/" element={<CourseRoutes />} />
      <Route path="/course/*" element={<CourseRoutes />} />
      <Route path="/users/*" element={<UsersRoutes />} />
      <Route path="*" element={<Navigate to="/course" />} />
    </Route>
    <Route path="" element={<AdminLayouts/>}>
      <Route path="/" element={<AdminRoutes/>}/>
      <Route path="/adminprofile/*" element={<AdminRoutes/>}>
        <Route path="information/*" element={<InformationRoutes/>} />
        <Route path="device/*" element={<DeviceRoutes/>} />
        <Route path="profile/*" element={<Profile/>} />
      </Route>
    </Route>
  </Routes>
  }else {
    return <Routes>
      <Route path="" element={<UsersLayouts/>}>
        <Route path="/" element={<UserCourseRoutes/>} />
        <Route path="/usercourse/*" element={<UserCourseRoutes/>} />
        <Route path="/userexam/*" element={<UserExamRoutes/>} />
        <Route path="/*" element={<Navigate to={"/usercourse"} />} />
      </Route>
        <Route path="/userprofile/*" element={<UserProfileRoutes/>} />
    </Routes>
  }
});
