import axios from "axios";
import { observer } from "mobx-react-lite";

const request = axios.create({ baseURL: import.meta.env.VITE_BASE_URL, });

observer(request.interceptors.request.use(config => {
  config.headers["Content-Type"] = "application/json";
  return config;
}))

export default request;
