import { Outlet } from "react-router-dom";
import { Box } from "@chakra-ui/react";
import cls from './styles.module.scss';
import { DashboardAdminProfile } from "components/DashboardAdminProfile";
import { HeaderAdmin } from "components/HeaderAdmin";

export const AdminLayouts = () => {
       return <Box display='flex'>
        <DashboardAdminProfile/>
        <Box width='100%'>
          <HeaderAdmin/>
          <Outlet className={cls.outlet} />
        </Box>

     </Box>
}
