import { authStore } from "store/auth.store"

export const useDashboardAdminProfileProps = () => {

       const name = authStore.userData?.user_data?.first_name
       const email = authStore.userData?.user_data?.email

       return {
              name,
              email,
       }
}