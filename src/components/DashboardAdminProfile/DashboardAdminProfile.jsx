import { useDashboardAdminProfileProps } from "./useDashboardAdminProfileProps"
import cls from './styles.module.scss';
import admin from '../../assets/images/admin.jpg'
import { NavLink } from "react-router-dom"
import { Box, Image } from "@chakra-ui/react"

export const DashboardAdminProfile = () => {

       const { name, email } = useDashboardAdminProfileProps()

       return               <>
       <aside className={cls.aside}>
         <Box className={cls.hero_section}>
         <Box className={cls.hero}>
            <h2 className={cls.site_heading}>
                <NavLink to={'/course'}>LMS</NavLink>
            </h2>
             <span className="material-symbols-outlined hamburger">menu_open</span>
         </Box>
         </Box>
         <ul className={cls.sidebar}>
             <li className={cls.sidebar_item}>
                 <NavLink className={cls.sidebar_link} to={'/adminprofile/information'}>
                 <span className={cls.sidebar_span}>
                 <span className="material-symbols-outlined">error</span>
                 </span>
                 <h4 className={cls.sideber_title}>Данные</h4>
                 </NavLink>
             </li>
             <li className={cls.sidebar_item}>
                 <NavLink className={cls.sidebar_link} to={'/adminprofile/device'}>
                     <span className={cls.sidebar_span}>
                     <span className="material-symbols-outlined">devices</span>
                     </span>
                 <h4 className={cls.sideber_title}>Устройства</h4>
                 </NavLink>
             </li>
             <li className={cls.sidebar_item}>
                 <NavLink className={cls.sidebar_link} to={'/adminprofile/profile'}>
                     <span className={cls.sidebar_span}>
                         <span className="material-symbols-outlined">logout</span>
                     </span>
                 <h4 className={cls.sideber_title}>Выйти</h4>
                 </NavLink>
             </li>
         </ul>
         <Box className={cls.down}>
            <Box className={cls.down_section}>
                <span className={cls.admin_picture}>
                    <Image className={cls.admin_img} src={admin} />
                </span>
                <Box className={cls.name_section}>
                    <h5 className={cls.user_name}>{name}</h5>
                    <p className={cls.user_email}>{email}</p>
                </Box>
            </Box>
         </Box>
     </aside>
       </>
}