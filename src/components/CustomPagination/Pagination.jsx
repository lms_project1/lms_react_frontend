import { Box } from '@chakra-ui/react'
import cls from './styles.module.scss'

export const CustomPagination = ({ totalPosts, postsPerPage, paginate }) => {
       const pages = []

       for(let i = 1; i <= Math.ceil(totalPosts/postsPerPage); i++){
              pages.push(i)
       }

       return <Box>
              <ul className={cls.pagination}>
                     {pages.map(number => {
                            return <li className={cls.page_item} key={number}>
                                   <a className={cls.page_link} href="!#" onClick={(evt) => paginate(evt, number)}>{number}</a>
                            </li>
                     })}
              </ul>

       </Box>
}
