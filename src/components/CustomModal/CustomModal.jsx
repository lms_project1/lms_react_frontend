import { Box, Button, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay } from "@chakra-ui/react";

export const CustomModal = ({
  isOpen,
  onClose,
  btnText1 = "Удалить",
  btnText2 = "Отменить",
  btnText3 = "Сохранить",
  callback,
  children,
  title,
  onDelete,
} ) => {

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent minBlockSize={560} maxInlineSize={600}>
        <ModalHeader>{title}</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          {children}
        </ModalBody>
        <ModalFooter display="flex" alignItems="center" justifyContent="space-between">
          <Button onClick={onDelete} colorScheme="red">{btnText1}</Button>
          <Box>
            <Button mr={3} onClick={onClose}>{btnText2}</Button>
            <Button colorScheme='blue' onClick={callback}>{btnText3}</Button>
          </Box>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};
