import { Box, Image } from "@chakra-ui/react";
import cls from "./styles.module.scss";

export const Header = () => {

  return <header className={cls.header}>
    <Box className={cls.container}>
      <Box className={cls.hero}>
        <Box className={cls.hero_icon}>
          <span className="material-symbols-outlined header_icons">notifications</span>
          <span className="material-symbols-outlined header_icons">chat_bubble</span>
        </Box>
      </Box>
    </Box>
  </header>
};
