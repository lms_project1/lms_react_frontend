import { NavLink } from "react-router-dom"
import { Box, Image } from "@chakra-ui/react"
import admin from '../../assets/images/admin.jpg'
import cls from './styles.module.scss'
import { useDashboardProps } from "./useDashboard"

export const Dashboard = () => {

    const { name, email } = useDashboardProps()

       return (
              <>
              <aside className={cls.aside}>
                <Box className={cls.hero_section}>
                <Box className={cls.hero}>
                    <h2 className={cls.site_heading}>
                        <a className={cls.site_name} href="#">LMS</a>
                    </h2>
                    <span className="material-symbols-outlined hamburger">menu_open</span>
                </Box>
                </Box>
                <ul className={cls.sidebar}>
                    <li className={cls.sidebar_item}>
                        <NavLink className={cls.sidebar_link} to={'/course'}>
                        <span className={cls.sidebar_span}>
                        <span className="material-symbols-outlined">school</span>
                        </span>
                        <h4 className={cls.sideber_title}>Курсы</h4>
                        </NavLink>
                    </li>
                    <li className={cls.sidebar_item}>
                        <NavLink className={cls.sidebar_link} to={'/users'}>
                            <span className={cls.sidebar_span}>
                                <span className="material-symbols-outlined">group</span>
                            </span>
                        <h4 className={cls.sideber_title}>Пользователи</h4>
                        </NavLink>
                    </li>
                </ul>
                <Box className={cls.down}>
                    <NavLink to={'/adminprofile'}>
                    <Box className={cls.down_section}>
                            <span className={cls.admin_picture}>
                                <Image className={cls.admin_img} src={admin} />
                            </span>
                            <Box className={cls.name_section}>
                                <h5 className={cls.user_name}>{name}</h5>
                                <p className={cls.user_email}>{email}</p>
                            </Box>
                    </Box>
                    </NavLink>
                </Box>
            </aside>
              </>
       )
}