import { Box } from '@chakra-ui/react'
import cls from './styles.module.scss'

export const HeaderAdmin = () => {
       return <header className={cls.header}>
       <Box className={cls.container}>
              <Box className={cls.hero}>
                     <Box>
                            <h3 className={cls.admin_header_heading}>Профиль</h3>
                     </Box>
                     <Box className={cls.hero_icon}>
                            <span className={cls.header_span}>
                                   <span className="material-symbols-outlined admin_header_icons">edit</span>
                            </span>
                            <span className={cls.header_span}>
                                   <span className="material-symbols-outlined admin_header_icons">emoji_flags</span>
                            </span>
                     </Box>
              </Box>
       </Box>
     </header>
}