import { Heading } from "@chakra-ui/react"
import { useMainProps } from "./useMainProps"
import cls from './styles.module.scss'

export const Main = () => {

       const {text} = useMainProps()

       return <Heading>{text}</Heading>
}