import { Heading } from "@chakra-ui/react";
import { useDetailProps } from "./useDetailProps";
import cls from './styles.module.scss';

export const Detail = () => {

       const { text } = useDetailProps()

       return <Heading>{text}</Heading>
}