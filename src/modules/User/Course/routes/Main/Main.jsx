import { Box, Button } from "@chakra-ui/react";
import { useMainProps } from "./useMainProps";
import cls from './styles.module.scss';

export const Main = () => {

       const { text } = useMainProps()

       return <Box className={cls.course_parent}>
              <Box className={cls.container}>
                     <Box className={cls.hero}>
                            <h5 className={cls.hero_heading}>What is IT, and what kind of spheres is in IT?</h5>
                            <p className={cls.hero_desc}>Get glimps of information about IT in order to choose sphere</p>
                            <Button className={cls.hero_btn}>Explore now</Button>
                     </Box>
              </Box>
       </Box>
}