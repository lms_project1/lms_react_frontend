import { Route, Routes } from "react-router-dom";
import { Profile } from "./LogOut";

export const UserProfileRoutes = () => {

  return <Routes>
    <Route path="" element={<Profile />} />
  </Routes>;
};
