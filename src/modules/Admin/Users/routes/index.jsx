import { Navigate, Route, Routes } from "react-router-dom";
import { Student } from "./Pages/Students";
import { Teacher } from "./Pages/Teacher";
import { Main } from "./Main";

export const UsersRoutes = () => {
  return <Routes>
    <Route path="" element={<Main />}>
      <Route path="/" element={<Student/>} />
      <Route index path="/student/*" element={<Student/>} />
      <Route path="/teacher/*" element={<Teacher/>} />
      <Route path="*" element={<Navigate to={'/users/student'}/>} />
    </Route>
  </Routes>;
};
