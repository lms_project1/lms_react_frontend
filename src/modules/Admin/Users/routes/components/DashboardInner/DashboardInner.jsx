import { Box } from "@chakra-ui/react"
import { Link, NavLink } from "react-router-dom"
import cls from './styles.module.scss'

export const DashboardInner = () => {
       return <Box>
                     <ul className={cls.course_list}>
                            <li className={cls.course_item}>
                                   <NavLink to={'/users/student'} className={cls.course_navlink}>
                                          Ученики
                                   </NavLink>
                            </li>
                            <li className={cls.course_item}>
                                   <NavLink to={'/users/teacher'} className={cls.course_navlink}>
                                          Менторы
                                   </NavLink>
                            </li>
                     </ul>
       </Box>
}