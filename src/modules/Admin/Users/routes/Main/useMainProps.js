import { useMutation } from "@tanstack/react-query";
import { useState } from "react";
import { useFieldArray, useForm } from "react-hook-form";
import request from "services/httpRequest";

export const useMainProps = () => {
       const [isOpen, setOpen] = useState(false);
       const[show, setShow] = useState(false)

       const { control, register ,handleSubmit, formState, reset } = useForm({
              defaultValues: {
                     students: []
              }
       })

       const {fields} = useFieldArray({
              control,
              name: "students"
       })

       const handleClick  = () => {
              setShow(!show)
       }

       const handleClose = () => {
              setOpen(false)
              reset()
       }

       const handleOpen = () => {
              setOpen(true)
       }

       // const [childData, setChildData] = useState({})
       // console.log(childData);

       const { mutate } = useMutation({mutationFn: (data) => request.post(`/api/v1/${data?.select}`, {
              first_name: data?.name,
              last_name: data?.surename,
              email: data?.adress,
              password: data?.password,
       })})

       const { mutateAsync } = useMutation({mutationFn: (data) => request.post('/api/v1/register', {
              first_name: data?.name,
              last_name: data?.surename,
              email: data?.adress,
              password: data?.password,
       })})

       const onSubmit = (data) => {

              if(data?.select === 'teachers'){
                     mutate(data)
              }else {
                     mutateAsync(data)
              }

              reset()
              setOpen(false)
       }




       return {
              handleOpen,
              isOpen,
              handleSubmit,
              onSubmit,
              handleClose,
              errors: formState.errors,
              register,
              show,
              handleClick,
              fields,
       }
}