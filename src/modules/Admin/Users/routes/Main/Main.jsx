import { Box, Button, FormControl, Input, InputGroup, InputRightElement, Select } from "@chakra-ui/react"
import { useMainProps } from "./useMainProps"
import cls from './styles.module.scss'
import { DashboardInner } from "../components/DashboardInner"
import { Outlet } from "react-router-dom"
import { CustomModal } from "components/CustomModal"
import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons"
import { CustomPagination } from "components/CustomPagination"
import clsx from "clsx"

export const Main = () => {

       const {handleOpen, isOpen, handleClose, handleSubmit, onSubmit, errors, register, handleClick, show, } = useMainProps()

       return <Box className={cls.course}>
       <Box className={cls.course_line}>
         <Box className={cls.container}>
           <Box className={cls.hero}>
             <h3 className={cls.hero_heading}>Обучение</h3>
             <Box className={cls.hero_right}>
              <span className={cls.hero_icon_border}>
                     <span className="material-symbols-outlined icon-span">download</span>
              </span>
              <span className={cls.hero_icon_border}>
                 <span className="material-symbols-outlined icon-span">search</span>
              </span>
              <span className={cls.hero_icon_border}>
                 <span className="material-symbols-outlined icon-span">filter_alt</span>
              </span>
              <Button onClick={handleOpen} className={cls.hero_btn} colorScheme="linkedin"><span className="material-symbols-outlined">add</span>Добавить</Button>
             </Box>
           </Box>
         </Box>
       </Box>

       <Box className={cls.dashboard_line}>
          <Box className={cls.container}>
            <DashboardInner/>
          </Box>
       </Box>

       <Box className={clsx(cls.course_line, cls.outlet_center)}>
          <Box className={clsx(cls.container, cls.outlet_center)}>
            <Outlet width={'100%'} />
          </Box>
       </Box>

       <CustomModal isOpen={isOpen} title={"Регистрация"} callback={handleSubmit(onSubmit)} onClose={handleClose}>
        <FormControl as="form">
          <Box className={cls.form_input_section}>
            <Input
              className={cls.form_input}
              placeholder="Ф.И.О"
              borderColor={errors['name'] ? 'red' : 'gray.300'}
              focusBorderColor={errors['name'] ? 'red.500' : 'gray.300'}
              {...register('name', {required: {value: true, message: "Введите Ф.И.О"}})} />
          </Box>

          <Box className={cls.form_input_section}>
            <Input
              className={cls.form_input}
              placeholder="Cемья"
              borderColor={errors['surename'] ? 'red' : 'gray.300'}
              focusBorderColor={errors['surename'] ? 'red.500' : 'gray.300'}
              {...register('surename', {required: {value: true, message: "Введите семья"}})} />
          </Box>

          <Box className={cls.form_input_section}>
            <Input
              type="email"
              className={cls.form_input}
              placeholder="Почта"
              borderColor={errors['adress'] ? 'red' : 'gray.300'}
              focusBorderColor={errors['adress'] ? 'red.500' : 'gray.300'}
              {...register('adress', {required: {value: true, message: "Введите почта"}})} />
          </Box>

          <Box className={cls.form_input_section}>
            <Input
              type="number"
              className={cls.form_input}
              placeholder="Номер телефона"
              // borderColor={errors['phone'] ? 'red' : 'gray.300'}
              // focusBorderColor={errors['phone'] ? 'red.500' : 'gray.300'}
              // {...register('phone', {required: {value: true, message: "Введите номер телефона"}})}
              />
          </Box>

          <Box className={cls.form_input_section}>
            <InputGroup>
            <Input
              type={show ? 'text' : 'password'}
              className={cls.form_input}
              placeholder="Пароль"
              borderColor={errors['password'] ? 'red' : 'gray.300'}
              focusBorderColor={errors['password'] ? 'red.500' : 'gray.300'}
              {...register('password', {required: {value: true, message: "Введите пароль"}})} />
              <InputRightElement>
                <Button h='1.75rem' size='sm' onClick={handleClick}>
                 {show ? <ViewOffIcon color='gray.300' /> : <ViewIcon color='gray.300' />}
                </Button>
              </InputRightElement>
            </InputGroup>
          </Box>
          
          <Select
            className={cls.form_input}
            placeholder='Выбрать'
            color={'#7e8b9f'}
            borderColor={errors['select'] ? 'red' : 'gray.300'}
            focusBorderColor={errors['select'] ? 'red.500' : 'gray.300'}
            {...register('select', {required: {value: true, message: "выбирать"}})}
          >
            <option value='student'>Студент</option>
            <option value='teachers'>Ментор</option>
          </Select>
        </FormControl>
       </CustomModal>
     
     </Box>;
}