import { Button } from "@chakra-ui/react";
import { useQuery } from "@tanstack/react-query";
import { useState } from "react";
import { useFieldArray, useForm } from "react-hook-form";
import request from "services/httpRequest";
import { authStore } from "store/auth.store";

export const useTeacherProps = () => {

  const [isOpen, setOpen] = useState(false);

  const { control, register, handleSubmit, reset, formState } = useForm()

  const { fields, append, remove } = useFieldArray({
    name: "first_name",
    control
  });


  const handleClose = () => {
    setOpen(false)
    reset()
}

const handleOpen = () => {
  setOpen(true)
}

const handleEditClick = (e, item) => {
  e.stopPropagation();
  handleOpen();
  reset({ names: item?.first_name });
};

const onSubmit = (data) => {
  setOpen(false)
}

  const columns = [
    {
      title: "№",
      dataIndex: 0,
      key: "n1",
    },
    {
      title: "Имя",
      dataIndex: "first_name",
      key: "name",
    },
    {
      title: "Mail",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "",
      dataIndex: "",
      key: "edit",
      className: 'tablespan',
      render: (item) => {
        return <Button ml='auto' padding="0" variant="ghost" onClick={(e) => handleEditClick(e, item)}>
          <span className="material-symbols-outlined edit-btn">edit_square</span>
        </Button>
      }
    }
  ];
  
  const getCreat = useQuery({queryKey: ['teachers'], queryFn: () => request.get(`/api/v1/teachers?offset=0&limit=10000`, data).then(res => res.data)})
  const data = getCreat?.data?.data

  authStore.createTeacherData(data)


    //  PAGINATION
    const [currentPage, setCurrentPage] = useState(1)
    const [postsPerPage, setPostsPerPage] = useState(5)

    const lastPostIndex = currentPage * postsPerPage
    const firstPostIndex = lastPostIndex - postsPerPage
    const currentPosts = data ? data.slice(firstPostIndex, lastPostIndex) : []
    const paginate = (evt, pageNumber) => {
      evt.preventDefault()
      setCurrentPage(pageNumber)
    }

    return {
           data,
           columns,
           currentPosts,
           postsPerPage,
           paginate,
           isOpen,
           handleSubmit,
           onSubmit,
           handleClose,
           errors: formState.errors,
           register
    }
};
