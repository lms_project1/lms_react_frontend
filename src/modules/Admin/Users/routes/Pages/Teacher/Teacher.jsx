import { Box, FormControl, Input } from "@chakra-ui/react";
import cls from "./styles.module.scss";
import { useTeacherProps } from "./useTeacherProps";
import Table from "rc-table";
import { CustomPagination } from "components/CustomPagination";
import { CustomModal } from "components/CustomModal";

export const Teacher = () => {

  const {
    columns,
    data,
    currentPosts,
    postsPerPage,
    paginate,
    isOpen,
    onSubmit,
    handleClose,
    handleSubmit,
    errors,
    register, } = useTeacherProps();

  return <Box className={cls.teacher_page}>
  <Box>
    <Box className={cls.table_radius}>
      <Table style={{borderRadius: '10px'}} className={cls.table} columns={columns} data={currentPosts} rowKey={data => data?.id} />
    </Box>
  </Box>

  <CustomPagination
    paginate={paginate}
    totalPosts={data ? data.length : 0}
    postsPerPage={postsPerPage} />

    <CustomModal isOpen={isOpen} title={"Изменить данные ученика"} callback={handleSubmit(onSubmit)} onClose={handleClose}>
      <FormControl as="form">
        <Input
          placeholder='name'
          borderColor={errors['names'] ? 'red' : 'gray.300'}
          focusBorderColor={errors['names'] ? 'red.500' : 'gray.300'}
          {...register('names', {required: {value: true, message: 'введите имя'}})} />
      </FormControl>
    </CustomModal>

  </Box>
};
