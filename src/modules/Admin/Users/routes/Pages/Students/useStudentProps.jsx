import { Button } from "@chakra-ui/react";
import { useMutation, useQuery } from "@tanstack/react-query";
import { useState } from "react";
import { useFieldArray, useForm } from "react-hook-form";
import request from "services/httpRequest";
import { useMainProps } from "../../Main/useMainProps";

export const useStudentProps = () => {

  // const [data, setData] = useState([])
  const [isOpen, setOpen] = useState(false);
  const [deleteId, setDeleteId] = useState(false);

  const { control, register, handleSubmit, reset, formState } = useForm({
    defaultValues: {
      students: []
    }
  })

  const { fields, append, remove } = useFieldArray({
    name: "students",
    control
  });


  const handleClose = () => {
    setOpen(false)
    reset()
}

const handleOpen = () => {
  setOpen(true)
}

const { mutate } = useMutation({mutationFn: (data) => request.delete(`/api/v1/students/${data}`)})

const handleDelete = () => {
    mutate(deleteId)
    setOpen(false)
}
       
  const handleEditClick = (e, item) => {
    e.stopPropagation();
    handleOpen();
    reset({ names: item?.first_name });
    setDeleteId(item?.id)
  };


  const onSubmit = (data) => {
    setOpen(false)
  }

const columns = [
       {
         title: "№",
         dataIndex: 'number',
         key: "n1",
       },
       {
         title: "Имя",
         dataIndex: "first_name",
         key: "name",
       },
       {
         title: "Mail",
         dataIndex: "email",
         key: "email",
       },
       {
        title: "",
        dataIndex: "",
        key: "edit",
        className: 'tablespan',
        render: (item) => {
          return <Button ml='auto' padding="0" variant="ghost" onClick={(e) => handleEditClick(e, item)}>
            <span className="material-symbols-outlined edit-btn">edit_square</span>
          </Button>
        }
       },
     ];

     
     const getCreat = useQuery({queryKey: ['course'], queryFn: () => request.get(`/api/v1/students?offset=0&limit=10000`).then(res => res.data)})

    //  const { childData } = useMainProps()
    //  console.log(childData);

    let data = getCreat?.data?.data ? getCreat?.data?.data : []


    
    //  PAGINATION
    const [currentPage, setCurrentPage] = useState(1)
    const [postsPerPage, setPostsPerPage] = useState(5)

    const lastPostIndex = currentPage * postsPerPage
    const firstPostIndex = lastPostIndex - postsPerPage
    const currentPosts = data ? data.slice(firstPostIndex, lastPostIndex) : []
    const paginate = (evt, pageNumber) => {
      evt.preventDefault()
      setCurrentPage(pageNumber)
    }

       return {
              data,
              columns,
              isOpen,
              handleSubmit,
              onSubmit,
              handleClose,
              register,
              errors: formState.errors,
              fields,
              append,
              currentPosts,
              postsPerPage,
              paginate,
              handleDelete,
       }
}