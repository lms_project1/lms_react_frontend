import cls from './styles.module.scss';
import { Box, FormControl, Input } from "@chakra-ui/react"
import Table from "rc-table"
import { useStudentProps } from './useStudentProps';
import { CustomModal } from 'components/CustomModal';
import { CustomPagination } from 'components/CustomPagination';

export const Student = () => {
       const {
        data,
        columns,
        isOpen,
        handleSubmit,
        onSubmit,
        handleClose,
        register,
        errors,
        currentPosts,
        postsPerPage,
        paginate,
        handleDelete,
        fields,
       } = useStudentProps()

       return <Box className={cls.student_page}>
       <Box>
       <Box className={cls.table_radius}>
         <Table style={{borderRadius: '10px'}} className={cls.table} columns={columns} data={currentPosts} rowKey={data => data?.id} />
       </Box>
        </Box>
        <CustomPagination
          paginate={paginate}
          totalPosts={data ? data.length : 0}
          postsPerPage={postsPerPage} />

        <CustomModal onDelete={handleDelete} isOpen={isOpen} title={"Изменить данные ученика"} callback={handleSubmit(onSubmit)} onClose={handleClose}>
          {fields.map((row, index) => {
            console.log(row);
            return (
              <FormControl as="form">
              <Input
                placeholder='name' 
                borderColor={errors['names'] ? 'red' : 'gray.300'}
                focusBorderColor={errors['names'] ? 'red.500' : 'gray.300'}
                {...register(`students.${index}.names`, {required: {value: true, message: 'введите имя'}})} />
              </FormControl>
            )
          })}
        </CustomModal>
     
       </Box>
}
