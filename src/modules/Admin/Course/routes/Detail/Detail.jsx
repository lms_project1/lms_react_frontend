import cls from "./styles.module.scss";
import { useCourseProps } from "./useCourseProps";


export const Detail = () => {

  const { text } = useCourseProps();

  return <h1 className={cls.title}>Detail: {text}</h1>;
};
