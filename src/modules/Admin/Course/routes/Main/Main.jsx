import { Box, Button, FormControl, Image, Input, InputGroup, InputRightElement, Select } from "@chakra-ui/react";
import cls from "./styles.module.scss";
import { useMainProps } from "./useMainProps";
import Table from "rc-table";
import { CustomModal } from "components/CustomModal";
import Backend from '../../../../../assets/images/Backend.png'
import { CustomPagination } from "components/CustomPagination";


export const Main = () => {

  const {
    columns,
    data,
    isOpen,
    onSubmit,
    handleSubmit,
    handleClose,
    handleOpen,
    register,
    errors,
    teacherData,
    currentPosts,
    postsPerPage,
    paginate,
  } = useMainProps();

  return <Box className={cls.course}>
  <Box className={cls.course_line}>
    <Box className={cls.container}>
      <Box className={cls.hero}>
        <h3 className={cls.hero_heading}>Обучение</h3>
        <Box className={cls.hero_right}>
          <span className={cls.hero_icon_border}>
            <span className="material-symbols-outlined icon-span">download</span>
          </span>
          <span className={cls.hero_icon_border}>
            <span className="material-symbols-outlined icon-span">search</span>
          </span>
          <span className={cls.hero_icon_border}>
            <span className="material-symbols-outlined icon-span">filter_alt</span>
          </span>
      <Button onClick={handleOpen} className={cls.hero_btn} colorScheme="linkedin"><span className="material-symbols-outlined">add</span>Добавить</Button>
        </Box>
      </Box>
    </Box>
  </Box>

  <Box className={cls.course_box}>
    <Box className={cls.container}>
      <Box className={cls.table_radius}>
        <Table className={cls.table} columns={columns} data={currentPosts} rowKey={data => data?.id} />
      </Box>
    </Box>

    <CustomPagination
      className={cls.paginate_course}
      paginate={paginate}
      totalPosts={data ? data.length : 0}
      postsPerPage={postsPerPage} />
  </Box>

  <CustomModal isOpen={isOpen} title={"Детали"} callback={handleSubmit(onSubmit)} onClose={handleClose}>
    <FormControl as='form'>
      <Box className={cls.form_picture_section}>
        <p className={cls.picture_title}>Загрузить фото</p>
        <Image className={cls.picture_img} src={Backend} />
      </Box>

      <Box className={cls.form_input_section}>
        <label className={cls.form_label} htmlFor="names">Teacher</label>
        <Select
        {...register('select', {required: {value: true, message: "tanlang"}})}>
          {teacherData.map((row, index) => {
            return <option key={index} value={row?.id}>{row?.first_name}</option>
          })}
        </Select>
      </Box>

      <Box className={cls.form_input_section}>
        <label className={cls.form_label} htmlFor="names">Название</label>
        <Input
        className={cls.form_input}
        id="names"
        placeholder="Введите название"
        borderColor={errors['name'] ? 'red' : 'gray.300'}
        focusBorderColor={errors['name'] ? 'red.500' : 'gray.300'}
        {...register('name', {required: {value: true, message: "Введите имя"}})} />
      </Box>

      <Box className={cls.form_input_section}>
        <label className={cls.form_label} htmlFor="descriptions">Описание</label>
        <Input
        className={cls.form_input}
        id="descriptions"
        placeholder="Введите описание"
        borderColor={errors['description'] ? 'red' : 'gray.300'}
        focusBorderColor={errors['description'] ? 'red.500' : 'gray.300'}
        {...register('description', {required: {value: true, message: "Введите описание"}})} />
      </Box>

      <Box className={cls.form_input_section}>
        <label className={cls.form_label} htmlFor="repeats">Повтояемость:</label>
        <Input
        className={cls.form_input}
        id="repeats"
        type="number"
        placeholder="Повтояемость"
        borderColor={errors['repeat'] ? 'red' : 'gray.300'}
        focusBorderColor={errors['repeat'] ? 'red.500' : 'gray.300'}
        {...register('repeat', {required: {value: true, message: "Введите повтояемость"}})} />
      </Box>

      <Box className={cls.form_input_section}>
        <label className={cls.form_label} htmlFor="lessons">Длителность урока</label>
        <Input
        className={cls.form_input}
        id="lessons"
        type="number"
        placeholder="Введите урока"
        borderColor={errors['lesson'] ? 'red' : 'gray.300'}
        focusBorderColor={errors['lesson'] ? 'red.500' : 'gray.300'}
        {...register('lesson', {required: {value: true, message: "Введите длителность урока"}})} />
      </Box>
      
      <Box className={cls.form_input_section}>
        <label className={cls.form_label} htmlFor="moneys">Сумма</label>
        <Input
        className={cls.form_input}
        type="number"
        id="moneys"
        placeholder="Введите сумма"
        borderColor={errors['money'] ? 'red' : 'gray.300'}
        focusBorderColor={errors['money'] ? 'red.500' : 'gray.300'}
        {...register('money', {required: {value: true, message: "Введите сумма"}})} />
      </Box>

      <Box className={cls.form_input_section}>
        <label className={cls.form_label} htmlFor="dates">Дата начало</label>
        <InputGroup>
          <Input
          className={cls.form_input}
          type="date"
          id="dates"
          placeholder="Введите дата"
          borderColor={errors['date'] ? 'red' : 'gray.300'}
          focusBorderColor={errors['date'] ? 'red.500' : 'gray.300'}
          {...register('date', {required: {value: true, message: "Введите дата"}})} />
        </InputGroup>
      </Box>
    </FormControl>
  </CustomModal>

</Box>;
};
