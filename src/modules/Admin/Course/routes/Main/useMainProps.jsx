import { useMutation, useQuery } from "@tanstack/react-query";
import { format } from "date-fns";
import { useEffect } from "react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import request from "services/httpRequest";
import { authStore } from "store/auth.store";

export const useMainProps = () => {

  const [child, setChild] = useState({})
  // const [data, setData] = useState([])
  const [isOpen, setOpen] = useState(false);

  const {register, handleSubmit, reset, formState} = useForm()

  const handleClose = () => {
    setOpen(false)
    reset()
  }

  const handleOpen = () => {
    setOpen(true)
  }

  const { mutate } = useMutation({mutationFn: (data) => request.post(`/api/v1/courses`, data)})
  const onSubmit = (data) => {
    const datas = {
        teacher_id: data?.select,
        name: data?.name,
        image:"https://miro.medium.com/v2/resize:fit:1200/1*mT7cuVx3htikWBf-L5n-Pg.png",
        category:data?.description,
        description:"this is go frontend course",
        price: data?.money - 0,
        start_date: data?.date,
        lesson_duration: data?.lesson - 0,
        number_of_lessons_per_week: data?.repeat - 0,
    }

    mutate(datas, {
      onSuccess: (res) => {
        setChild(res?.data)
      }
    })
    setOpen(false)
  }
  
const columns = [
  {
    title: "№",
    dataIndex: 'number',
    key: "n1",
    width: 10,
  },
  {
    title: "Название курса",
    dataIndex: "name",
    key: "name",
    width: 100,
  },
  {
    title: "Описание",
    dataIndex: "description",
    key: "description",
    width: 100,
  },
  {
    title: "Тип",
    dataIndex: "category",
    key: "category",
    width: 100,
  },
  {
    title: "Время",
    dataIndex: "lesson_duration",
    key: "lesson_duration",
    width: 100,
  },
  {
    title: "Кол-во документов",
    dataIndex: "number_of_lessons_per_week",
    key: "materials",
    width: 100,
  },
  {
    title: "Дата",
    dataIndex: "created_at",
    key: "date",
    width: 100,
    render: (item) => <span>{item ? format(new Date(item), "dd.MM.yyyy") : "" }</span>,
  },
  {
    title: "Оценка",
    dataIndex: "price",
    key: "price",
    width: 100,
  },
];

const getCreat = useQuery({queryKey: ['student'], queryFn: () => request.get(`/api/v1/courses?offset=0&limit=100`).then(res => res.data)})
const data = getCreat?.data?.courses


const teacherData = authStore.saveTeacherData


    //  PAGINATION
    const [currentPage, setCurrentPage] = useState(1)
    const [postsPerPage, setPostsPerPage] = useState(8)

    const lastPostIndex = currentPage * postsPerPage
    const firstPostIndex = lastPostIndex - postsPerPage
    const currentPosts = data ? data.slice(firstPostIndex, lastPostIndex) : []
    const paginate = (evt, pageNumber) => {
      evt.preventDefault()
      setCurrentPage(pageNumber)
    }

  return {
    columns,
    data: data ? data : [],
    isOpen,
    handleSubmit,
    onSubmit,
    handleClose,
    handleOpen,
    register,
    errors: formState.errors,
    teacherData,
    currentPosts,
    postsPerPage,
    paginate,
  };
};
