import { Heading } from "@chakra-ui/react"
import cls from './styles.module.scss'
import { useDetailProps } from "./useDetailProps"

export const Detail = () => {

       const {} = useDetailProps()

       return <Heading>Information detail</Heading>
}