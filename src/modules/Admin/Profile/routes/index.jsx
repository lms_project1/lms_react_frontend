import { Route, Routes } from "react-router-dom"
import { Profile } from "./Profile"
import { InformationRoutes } from "./Information"
import { DeviceRoutes } from "./Device"

export const AdminRoutes = () => {
       return <Routes>
              <Route index path="/*" element={<InformationRoutes/>} />
              <Route index path="/information/*" element={<InformationRoutes/>} />
              <Route path="/device/*" element={<DeviceRoutes/>} />
              <Route path="/profile/*" element={<Profile/>} />
              <Route path="*" element={<InformationRoutes/>} />
       </Routes>
}
