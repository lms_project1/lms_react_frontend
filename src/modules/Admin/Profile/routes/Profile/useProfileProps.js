import { authStore } from "store/auth.store"

export const useProfileProps = () => {

       const handleLogOut = () => {
              authStore.logout()
       }

       return {
              handleLogOut
       }
}