import { useMutation } from "@tanstack/react-query";
import { useEffect, useState } from "react"
import { useForm } from "react-hook-form"
import { useNavigate } from "react-router-dom"
import request from "services/httpRequest";
import { authStore } from "store/auth.store";

export const useCodeProps = () => {


       const [count, setCount] = useState(59);
       const [btn, setBtn] = useState('Зарегистрироваться')
       const [state, setState] = useState('')
       const [desible, setDesible] = useState(false)
       
       const {register, handleSubmit, formState} = useForm()
       useEffect(() => {
           if(count===0){
              setCount(null)
              setBtn("повторно отправить код")
              setState("код устарел")
              setDesible(true)
           }


           if (!count) return;
       
           const intervalId = setInterval(() => {
       
              setCount(count - 1);
           }, 1000);
       
           return () => clearInterval(intervalId);
         }, [count]);
       

       const navigate = useNavigate()

       const { mutate } = useMutation({mutationFn: (data) => request.post(`/api/v1/verify?email=${authStore?.isEmail?.data?.gmail}&code=${data?.code}`)})

       const onSubmit = (data) => {
              mutate(data, {
                     onSuccess: (res) => {
                                   navigate('/auth/register')
                     },
                     onError: (error) => {
                            setState('ошибка кода')
                     }
              })
       }

       return {
              register,
              handleSubmit,
              onSubmit,
              count,
              errors: formState.errors,
              btn,
              state,
              desible,
       }
}