import { Box, Button, FormControl, Heading, Image, Input, InputGroup, InputLeftElement, InputRightElement, } from "@chakra-ui/react";
import cls from './styles.module.scss';
import authIcon from '../../../../assets/images/auth.png'
import { useCodeProps } from "./useCodeProps";
import { Link } from "react-router-dom";

export const Emailcode = () => {

       const {handleSubmit, onSubmit, register, count, errors, btn, state, desible } = useCodeProps()

       return  <Box className={cls.register}>
       <Box className={cls.auth_left}>
         <Box className={cls.left_section}>
         <Heading className={cls.auth_left_heading}>Learning Management system</Heading>
         <Image
           src={authIcon}
         />
         </Box>
       </Box>
     <Box className={cls.aut_right}>
       <Box className={cls.auth_right_section}>
       <FormControl onSubmit={handleSubmit(onSubmit)} className={cls.form} as="form">
       <h5 className={cls.verify_heading} mb={2}>Пройдите регистрацию</h5>
        
        <ul className={cls.list}>
         <li className={cls.item_one}>
           <span className={cls.item_span}>1</span>
         </li>
         <li className={cls.item_two}>
           <span className={cls.item_span}>2</span>
         </li>
        </ul>
 

        <Box className={cls.form_box}>

         <Box className={cls.input_box} mt={5}>
           <label className={cls.label} htmlFor="code">Код потвреждения</label>
           <InputGroup mt={1.5}>
              <Input
              type='number'
              id='code'
              placeholder='Введите код'
              borderColor={errors['code'] ? 'red' : 'gray.300'}
              focusBorderColor={errors['code'] ? 'red.500' : 'gray.300'}
              {...register('code', {required: {value: true, message: "Kод не введен!"}})} />
            <InputRightElement width='4.5rem'>
              <span>{count}</span>
            </InputRightElement>
           </InputGroup>
            {errors['code'] && <span className={cls.errors}>{errors['code']?.message}</span>} 
            <span className={cls.errors}>{state}</span>
         </Box>

          <Button isDisabled={desible} className={cls.auth_btn} colorScheme="blue" mt={7} type="submit">{btn}</Button>
        </Box>
      </FormControl>
      {desible && <Box display="flex" justifyContent="center" color="dodgerblue" mt={3}><Link to="/auth/verify">Повторная отправка</Link></Box>}
      </Box>
     </Box>

    </Box>;
}