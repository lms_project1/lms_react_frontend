import { Box, Button, FormControl, Heading, Image, Input, InputGroup, InputLeftElement, InputRightElement } from "@chakra-ui/react"
import { useForgetProps } from "./useForgetProps"
import cls from './styles.module.scss'
import { EmailIcon, UnlockIcon, ViewIcon, ViewOffIcon } from "@chakra-ui/icons"
import authIcon from '../../../../assets/images/auth.png'

export const Forget = () => {
       const {handleSubmit, onSubmit, register, errors, state, } = useForgetProps()

       return  <Box className={cls.register}>
       <Box className={cls.auth_left}>
         <Box className={cls.left_section}>
         <Heading className={cls.auth_left_heading}>Learning Management system</Heading>
         <Image
           src={authIcon}
         />
         </Box>      
       </Box>
     <Box className={cls.aut_right}>
       <Box className={cls.auth_right_section}>
       <FormControl onSubmit={handleSubmit(onSubmit)} className={cls.form} as="form">
       <h5 className={cls.heading_name} mb={2}>Восстановление пароля</h5>
        <Box className={cls.form_box}>

         <Box className={cls.input_box} mt={5}>
           <label className={cls.label} htmlFor="gmail">Email<span className={cls.star}>*</span></label>
           <InputGroup mt={1.5}>
             <InputLeftElement pointerEvents='none'>
               <EmailIcon color='gray.300' />
             </InputLeftElement>
             <Input 
             type='email'
             id='gmail'
             placeholder='Электронная почта'
             borderColor={errors['gmail'] ? 'red' : 'gray.300'}
             focusBorderColor={errors['gmail'] ? 'red.500' : 'gray.300'}
             {...register('gmail', {required: {value: true, message: "Введите адрес электронной почты!"}})} />
           </InputGroup>
           {errors['gmail'] && <span style={{color: 'red'}}>{errors['gmail'].message}</span>}
         </Box>

         {state && <Box className={cls.input_box} mt={5}>
           <label className={cls.label} htmlFor="parol">Код активации<span className={cls.star}>*</span></label>
         <InputGroup mt={1.5}>
           <Input
           type="number"
           id='parol'
           placeholder='Код активации'
           borderColor={errors['password'] ? 'red' : 'gray.300'}
           focusBorderColor={errors['password'] ? 'red.500' : 'gray.300'}
           {...register('password', {required: {value: true, message: "Введите пароль"}})} />
         </InputGroup>
         {errors['password'] && <span style={{color: 'red'}}>{errors['password'].message}</span>}
         </Box>}

         <Box className={cls.link} mt={3}>{state ? "Отправить код еще раз" : "Введите электронная почта чтобы получить код активации"}</Box>

          <Button className={cls.auth_btn} colorScheme="blue" mt={7} type="submit">Получить код активации</Button>
        </Box>
      </FormControl>
      </Box>
     </Box>

    </Box>;
}