
import { useMutation } from "@tanstack/react-query"
import axios from "axios"
import { useState } from "react"
import { useForm } from "react-hook-form"
import { useNavigate } from "react-router-dom"
import request from "services/httpRequest"
import { authStore } from "store/auth.store"

export const useForgetProps = () => {

       const [state, setState] = useState(false)

       const navigate = useNavigate()

       const {register, handleSubmit, formState} = useForm()

       const { mutate } = useMutation({mutationFn: (data) => request.post(`/api/v1/sendcode?email=${data?.gmail}`)})

       const { mutateAsync } = useMutation({mutationFn: (data) => request.post(`/api/v1/verify?email=${data?.gmail}&code=${data?.password}`)})

       const onSubmit = (data) => {
              console.log(data);
              setState(true)
              if(state) {
                     mutateAsync(data, {
                            onSuccess: (res) => {
                                   navigate('/auth/password')
                            },
                            onError: (error) => {
                                   alert('ошибка кода')
                            }
                     })
              }else {
                     axios.get(`https://lms-service-xhvq.onrender.com/userbyemail/${data?.gmail}`)
                     .then((res) => {
                            mutate(data, {
                                   onSuccess: () => {
                                          authStore.saveForget(data)
                                   }
                            })
                        }).catch((err) => {
                            alert('Bu email mavjud emas')
                        })

              }
       }

       return {
              handleSubmit,
              onSubmit,
              register,
              errors: formState.errors,
              state,
       }
}