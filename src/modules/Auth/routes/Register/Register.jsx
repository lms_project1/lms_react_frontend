import { Box, Button, FormControl, Heading, Image, Input, InputGroup, InputLeftElement, InputRightElement, } from '@chakra-ui/react';
import cls from './styles.module.scss';
import { useRegisterProps } from "./useRegisterProps"
import { Link } from 'react-router-dom';
import { EmailIcon, LockIcon, UnlockIcon, ViewIcon, ViewOffIcon } from '@chakra-ui/icons';
import autIcon from '../../../../assets/images/auth.png';

export const Register = () => {

       const {show, handleClick, handleSubmit, onSubmit, register, errors, defautlEmail, defautName } = useRegisterProps()



       return <Box className={cls.register}>
        <Box className={cls.auth_left}>
          <Box className={cls.left_section}>
          <Heading className={cls.auth_left_heading}>Learning Management system</Heading>
          <Image
            src={autIcon}
          />
          </Box>
        </Box>
      <Box className={cls.aut_right}>
        <Box className={cls.auth_right_section}>
        <FormControl className={cls.form} as="form" onSubmit={handleSubmit(onSubmit)}>
         <Heading textAlign="center" mb={2}>Регистратор</Heading>
         <Box className={cls.form_box}>
          <Box className={cls.input_box} mt={5}>
            <label className={cls.label} htmlFor='name'>Имя<span className={cls.star}>*</span></label>
            <Input 
            type="text" 
            id='name' 
            placeholder='Имя' 
            defaultValue={defautName}
            mt={1.5}
            borderColor={errors['register_name'] ? 'red' : 'gray.300'}
            focusBorderColor={errors['register_name'] ? 'red.500' : 'gray.300'}
            {...register('register_name', {required: {value: true, message: 'Введите имя'}})} />
          </Box>

          <Box className={cls.input_box} mt={5}>
            <label className={cls.label} htmlFor='sureName'>Фамилия<span className={cls.star}>*</span></label>
            <Input 
            type="text" 
            id='sureName' 
            placeholder='Фамилия' 
            mt={1.5} 
            borderColor={errors['register_surename'] ? 'red' : 'gray.300'}
            focusBorderColor={errors['register_surename'] ? 'red.500' : 'gray.300'}
            {...register('register_surename', {required: {value: true, message: 'Введите фамилию'}})} />
           </Box>

          <Box className={cls.input_box} mt={5}>
            <label className={cls.label} htmlFor="gmail">Электронная почта<span className={cls.star}>*</span></label>
            <InputGroup mt={1.5}>
              <InputLeftElement pointerEvents='none'>
                <EmailIcon  color={errors['email'] ? 'red': 'gray.300'} />
              </InputLeftElement>
              <Input
              type='email'
              id='gmail'
              placeholder='электронная почта'
              defaultValue={defautlEmail}
              borderColor={errors['email'] ? 'red' : 'gray.300'}
              focusBorderColor={errors['email'] ? 'red.500' : 'gray.300'}
              {...register('email', {required: {value: true, message: "Введите электронная почта"}})} />
            </InputGroup>
          </Box>

          <Box className={cls.input_box} mt={5}>
            <label className={cls.label} htmlFor="parol">Пароль<span className={cls.star}>*</span></label>
          <InputGroup mt={1.5}>
            <InputLeftElement pointerEvents='none'>
              <LockIcon color={errors['password'] ? 'red': 'gray.300'} />
            </InputLeftElement>

            <Input
            type={show ? "text" : "password"}
            id='parol'
            placeholder='Пароль'  
            borderColor={errors['password'] ? 'red' : 'gray.300'}
            focusBorderColor={errors['password'] ? 'red.500' : 'gray.300'}
            {...register('password', {required: {value: true, message: 'введите пароль'}})} />

            <InputRightElement width='4.5rem'>
              <Button h='1.75rem' size='sm' onClick={handleClick}>
                {show ? <ViewOffIcon color={errors['password'] ? 'red': 'gray.300'}  /> : <ViewIcon/>}
              </Button>
            </InputRightElement>
          </InputGroup>
          {errors['password'] && <span className={cls.result_password}>{errors['password'].message}</span>}

          {/* <Box>
          <InputGroup mt={1.5}>
            <InputLeftElement pointerEvents='none'>
              <LockIcon color='gray.300' />
            </InputLeftElement>

            <Input
            type={shows ? "text" : "password"}
            id='repeat-parol'
            placeholder='Repeat password'
            {...register('repeat_password', {required: {value: true, message: "Parol mos emas"}})} />

            <InputRightElement width='4.5rem'>
              <Button h='1.75rem' size='sm' onClick={handleClicks}>
                {shows ? <ViewOffIcon color='gray.300' /> : <ViewIcon color='gray.300' />}
              </Button>
            </InputRightElement>
          </InputGroup>
          {state && <span>{errors['repeat_password']?.message}</span>}
          </Box>. */}
          </Box>

           <Button className={cls.auth_btn} colorScheme="blue" mt={7} type="submit">Войти</Button>
         </Box>
       </FormControl>
       <Box display="flex" justifyContent="center" color="dodgerblue" mt={3}><Link to="/auth/login">Авторизоваться</Link></Box>
       </Box>
      </Box>

     </Box>;
}