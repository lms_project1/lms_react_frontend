import { useMutation } from "@tanstack/react-query";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import request from "services/httpRequest";
import { authStore } from "store/auth.store";

export const useRegisterProps = () => {
       
       const [show, setShow] = useState(false);
       const handleClick = () => setShow(!show);

       const navigate = useNavigate()


       const {register, handleSubmit, formState} = useForm()

       const { mutate } = useMutation({ mutationFn: (data) => request.post('/api/v1/register', data)})

       const defautlEmail = authStore.isEmail?.data?.gmail
       const defautName = authStore.isEmail?.data?.user

       const onSubmit = (data) => {
                     mutate({
                            first_name: data?.register_name,
                            last_name: data?.register_surename,
                            email: data?.email,
                            password: data?.password,
                        }, {
                            onSuccess: () => {
                                   navigate('/usercourse')
                            }
                        })
       }


       return {
              handleClick,
              show,
              register,
              handleSubmit,
              onSubmit,
              errors: formState.errors,
              defautlEmail,
              defautName,
       }
}
