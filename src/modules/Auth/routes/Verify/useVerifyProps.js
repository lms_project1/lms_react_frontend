import { useMutation } from "@tanstack/react-query";
import axios from "axios";
import { useState } from "react";
import { useForm } from "react-hook-form"
import { useNavigate } from "react-router-dom";
import request from "services/httpRequest";
import { authStore } from "store/auth.store";

export const useVerifyProps = () => {

       const [state, setState] = useState(false)


       const { handleSubmit, register, formState, } = useForm()

       const navigate = useNavigate()

       
       const { mutate } = useMutation({mutationFn: (data) => request.post(`/api/v1/sendcode?email=${data?.gmail}`)})

       const onSubmit = (data) => {

              axios.get(`https://lms-service-xhvq.onrender.com/api/v1/userbyemail/${data?.gmail}`)
              .then((res) => {
                     setState("Bu email mavjud")
                 }).catch((err) => {
                     mutate(data, {
                            onSuccess: () => {
                            navigate('/auth/emailcode')
                            authStore.saveEmail({data})
                            authStore.saveName({})
                            },
                     })
                 })
}


       return {
              handleSubmit,
              register,
              onSubmit,
              errors: formState.errors,
              state,
       }
}