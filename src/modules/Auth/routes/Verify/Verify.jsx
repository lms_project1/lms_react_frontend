import { Box, Button, FormControl, Heading, Image, Input, InputGroup, InputLeftElement } from "@chakra-ui/react";
import { useVerifyProps } from "./useVerifyProps";
import { EmailIcon } from "@chakra-ui/icons";
import cls from './styles.module.scss';
import authIcon from '../../../../assets/images/auth.png';

export const Verify = () => {

       const { handleSubmit, register, onSubmit, errors, state } = useVerifyProps()

       return  <Box className={cls.register}>
       <Box className={cls.auth_left}>
         <Box className={cls.left_section}>
         <Heading className={cls.auth_left_heading}>Learning Management system</Heading>
         <Image
           src={authIcon}
         />
         </Box>
       </Box>
     <Box className={cls.aut_right}>
       <Box className={cls.auth_right_section}>
       <FormControl onSubmit={handleSubmit(onSubmit)} className={cls.form} as="form">
        <h5 className={cls.heading_name} mb={2}>Пройдите регистрацию</h5>
        
       <ul className={cls.list}>
        <li className={cls.item_one}>
          <span className={cls.item_span}>1</span>
        </li>
        <li className={cls.item_two}>
          <span className={cls.item_span_two}>2</span>
        </li>
       </ul>

        <Box className={cls.form_box}>

          <Box className={cls.input_box} mt={5}>
            <label htmlFor="user" className={cls.label}>Ваше Ф.И.О<span className={cls.star}>*</span></label>
            <InputGroup mt={1.5}>
             <InputLeftElement pointerEvents='none'>
               <span color={errors['user'] ? 'red' : 'gray.300'}>
                <span color="red" className="material-symbols-outlined verify_icon">person</span>
               </span>
             </InputLeftElement>
             <Input 
             type='text'
             id='user'
             placeholder='Введите имя'
             borderColor={errors['user'] ? 'red' : 'gray.300'}
             focusBorderColor={errors['user'] ? 'red.500' : 'gray.300'}
             {...register('user', {required: {value: true, message: "Name kiritilmadi!"}})} />
            </InputGroup>
           {errors['user'] && <span className={cls.errors}>{errors['user'].message}</span>}
           {/* {state && <span className={cls.errors}>{state}</span>} */}
          </Box>

         <Box className={cls.input_box} mt={5}>
           <label className={cls.label} htmlFor="gmail">Укажите в e-mail<span className={cls.star}>*</span></label>
           <InputGroup mt={1.5}>
             <InputLeftElement pointerEvents='none'>
               <EmailIcon color={errors['gmail'] ? 'red' : 'gray.300'} />
             </InputLeftElement>
             <Input
             type='email' 
             id='gmail' 
             placeholder='Введите  e-mail'
             borderColor={errors['gmail'] ? 'red' : 'gray.300'}
             focusBorderColor={errors['gmail'] ? 'red.500' : 'gray.300'}
             {...register('gmail', {required: {value: true, message: "Email kiritilmadi!"}})} />
           </InputGroup>
           {errors['gmail'] && <span className={cls.errors}>{errors['gmail'].message}</span>}
           {state && <span className={cls.errors}>{state}</span>}
         </Box>

          <Button className={cls.auth_btn} colorScheme="blue" mt={7} type="submit">Следующий шаг</Button>
        </Box>
      </FormControl>
      </Box>
     </Box>

    </Box>;
}