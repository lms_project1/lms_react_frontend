import { Box, Button, FormControl, Heading, Image, Input, InputGroup, InputLeftElement, InputRightElement } from "@chakra-ui/react"
import { usePasswordProps } from "./usePasswordProps"
import cls from './styles.module.scss'
import { EmailIcon, UnlockIcon, ViewIcon, ViewOffIcon } from "@chakra-ui/icons"
import authIcon from '../../../../assets/images/auth.png'

export const Password = () => {
       const { show, handleClick, register, handleSubmit, onSubmit, handleClicks, shows, errors, state} = usePasswordProps()
       return <Box className={cls.register}>
       <Box className={cls.auth_left}>
         <Box className={cls.left_section}>
         <Heading className={cls.auth_left_heading}>Learning Management system</Heading>
         <Image
           src={authIcon}
         />
         </Box>
       </Box>
     <Box className={cls.aut_right}>
       <Box className={cls.auth_right_section}>
       <FormControl onSubmit={handleSubmit(onSubmit)} className={cls.form} as="form">
       <h5 className={cls.heading_name} mb={2}>Пройдите регистрацию</h5>
        <Box className={cls.form_box}>

        <Box className={cls.input_box} mt={5}>
           <label className={cls.label} htmlFor="parol">Создать код<span className={cls.star}>*</span></label>
         <InputGroup mt={1.5}>
           <InputLeftElement pointerEvents='none'>
             <UnlockIcon color='gray.300' />
           </InputLeftElement>
           <Input
           type={show ? "text" : "password"}
           id='parol'
           placeholder='Создать код'
           borderColor={errors['password'] ? 'red' : 'gray.300'}
           focusBorderColor={errors['password'] ? 'red.500' : 'gray.300'}
           {...register('password', {required: {value: true, message: "введите код"}})} />
           <InputRightElement width='4.5rem'>
             <Button h='1.75rem' size='sm' onClick={handleClick}>
               {show ? <ViewOffIcon color='gray.300' /> : <ViewIcon color='gray.300' />}
             </Button>
           </InputRightElement>
         </InputGroup>
         {errors['password'] && <span style={{color: 'red'}}>{errors['password'].message}</span>}
         </Box>


         <Box className={cls.input_box} mt={5}>
           <label className={cls.label} htmlFor="parol">Повторите код<span className={cls.star}>*</span></label>
         <InputGroup mt={1.5}>
           <InputLeftElement pointerEvents='none'>
             <UnlockIcon color='gray.300' />
           </InputLeftElement>
           <Input
           type={shows ? "text" : "password"}
           id='parol'
           placeholder='Повторите код'
           borderColor={errors['repeat'] ? 'red' : 'gray.300'}
           focusBorderColor={errors['repeat'] ? 'red.500' : 'gray.300'}
           {...register('repeat', {required: {value: true, message: "Повторите код"}})} />
           <InputRightElement width='4.5rem'>
             <Button h='1.75rem' size='sm' onClick={handleClicks}>
               {shows ? <ViewOffIcon color='gray.300' /> : <ViewIcon color='gray.300' />}
             </Button>
           </InputRightElement>
         </InputGroup>
         {errors['repeat'] &&  <span style={{color: 'red'}}>{errors['repeat'].message}</span>}
         {state && <span className={cls.link}>повторить ошибку</span>}
         </Box>

          <Button className={cls.auth_btn} colorScheme="blue" mt={7} type="submit">Изменить пароль</Button>
        </Box>
      </FormControl>
      </Box>
     </Box>

    </Box>;
}