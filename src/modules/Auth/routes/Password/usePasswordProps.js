import { useMutation } from "@tanstack/react-query"
import { toJS } from "mobx"
import { useState } from "react"
import { useForm } from "react-hook-form"
import { useNavigate } from "react-router-dom"
import request from "services/httpRequest"
import { authStore } from "store/auth.store"

export const usePasswordProps = () => {

       const [ show, setShow ] = useState(false)
       const handleClick = () => setShow(!show)

       const [ shows, setShows ] = useState(false)
       const handleClicks = () => setShows(!shows)

       const [state, setState] = useState(false)

       const {register, handleSubmit, formState, } = useForm()

       const navigate = useNavigate()

       const emailSave = authStore.isForgetEmail?.gmail

       const { mutate } = useMutation({mutationFn: (data) => request.put(`/api/v1/changepassword?email=${emailSave}&password=${data?.password}`)})
       
       const onSubmit = (data) => {
              if(data?.password === data?.repeat){
                     mutate(data, {
                            onSuccess: (res) => {
                                   navigate('auth/login')
                            }
                     })
              }else {
                     setState(true)
              }

       }

       return {
              show,
              handleClick,
              handleSubmit,
              register,
              onSubmit,
              handleClicks,
              shows,
              errors: formState.errors,
              state,
       }
}