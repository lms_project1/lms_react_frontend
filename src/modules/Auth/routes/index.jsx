import { Navigate, Route, Routes } from "react-router-dom"
import { Register } from "./Register"
import { Logins } from "./Logins"
import { Verify } from "./Verify"
import { Emailcode } from "./Emailcode"
import { Forget } from "./Forget"
import { Password } from "./Password"

export const AuthRoutes = () => {
       return <Routes>
              <Route index path="verify" element={<Verify/>} />
              <Route index path="emailcode" element={<Emailcode/>} />
              <Route path="login" element={<Logins/>}/>
              <Route path="forget" element={<Forget/>} />
              <Route path="password" element={<Password/>} />
              <Route path="register" element={<Register/>} />
              <Route path="*" element={<Navigate to="login" />} />
       </Routes>
}