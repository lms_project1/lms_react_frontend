import { useMutation, useQuery } from "@tanstack/react-query"
import { toJS } from "mobx"
import { useState } from "react"
import { useForm } from "react-hook-form"
import request from "services/httpRequest"
import { authStore } from "store/auth.store"

export const useLoginsProps = () => {

       const [show, setShow] = useState(false)
       const handleClick = () => setShow(!show)

       const [state, setState] = useState(false)

       const { register, handleSubmit, formState } = useForm()

       const { mutate } = useMutation({ mutationFn: (data) => request.post(`/api/v1/login?email=${data?.gmail}&password=${data?.password}`, data)})


       const onSubmit = (data) => {
              mutate(data, {
                     onSuccess: (res) => {
                            authStore.login()
                            authStore.saveUserData(res?.data)
                     },
                     onError: (error) => {
                            setState(true)
                     }
              })
       }

       return {
              show,
              handleClick,
              register,
              handleSubmit,
              onSubmit,
              errors: formState.errors,
              state,
       }
}