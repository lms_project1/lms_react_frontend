import { Box, Button, FormControl, Heading, Image, Input, InputGroup, InputLeftElement, InputRightElement } from "@chakra-ui/react"
import { useLoginsProps } from "./useLoginsProps";
import cls from './styles.module.scss'
import { Link } from "react-router-dom";
import { EmailIcon, UnlockIcon, ViewIcon, ViewOffIcon } from "@chakra-ui/icons";
import authIcon from '../../../../assets/images/auth.png';

export const Logins = () => {

       const { show, handleClick, register, handleSubmit, onSubmit, errors, state } = useLoginsProps()

       return <Box className={cls.register}>
       <Box className={cls.auth_left}>
         <Box className={cls.left_section}>
         <Heading className={cls.auth_left_heading}>Learning Management system</Heading>
         <Image
           src={authIcon}
         />
         </Box>
       </Box>
     <Box className={cls.aut_right}>
       <Box className={cls.auth_right_section}>
       <FormControl onSubmit={handleSubmit(onSubmit)} className={cls.form} as="form">
       <h5 className={cls.heading_name} mb={2}>Вход в платформу</h5>

        <Box className={cls.form_box}>

         <Box className={cls.input_box} mt={5}>
           <label className={cls.label} htmlFor="gmail">Email<span className={cls.star}>*</span></label>
           <InputGroup mt={1.5}>
             <InputLeftElement pointerEvents='none'>
               <EmailIcon color={errors['gmail'] ? 'red': 'gray.300'} />
             </InputLeftElement>
             <Input 
             type='email' 
             id='gmail' 
             placeholder='Электронная почта' 
             borderColor={errors['gmail'] ? 'red' : 'gray.300'}
             focusBorderColor={errors['gmail'] ? 'red.500' : 'gray.300'}
             {...register('gmail', {required: {value: true, message: 'Введите електронная почта !'}})} />
           </InputGroup>
         </Box>

         <Box className={cls.input_box} mt={5}>
           <label className={cls.label} htmlFor="parol">Пароль<span className={cls.star}>*</span></label>
         <InputGroup mt={1.5}>
           <InputLeftElement pointerEvents='none'>
             <UnlockIcon color={errors['password'] ? 'red': 'gray.300'} />
           </InputLeftElement>
           <Input
           type={show ? "text" : "password"}
           id='parol'
           placeholder='Пароль'
           borderColor={errors['password'] ? 'red' : 'gray.300'}
           focusBorderColor={errors['password'] ? 'red.500' : 'gray.300'}
           {...register('password', {required: {value: true, message: 'Введите пароль!'}})} />
           <InputRightElement width='4.5rem'>
             <Button h='1.75rem' size='sm' onClick={handleClick}>
               {show ? <ViewOffIcon color='gray.300' /> : <ViewIcon color='gray.300' />}
             </Button>
           </InputRightElement>
         </InputGroup>
         </Box>
         {state && <span className={cls.feedback}>Ошибка пароля</span>} 
          <Box className={cls.link} color="dodgerblue" mt={3}><Link to={'/auth/forget'}>Забыли пароль?</Link></Box>

          <Button className={cls.auth_btn} colorScheme="blue" mt={7} type="submit">Войти</Button>
        </Box>
      </FormControl>
      <Box className={cls.label} display="flex" justifyContent="center" color="dodgerblue" mt={3}><Link to="/auth/verify">Зарегистрироваться</Link></Box>
      </Box>
     </Box>

    </Box>;
}