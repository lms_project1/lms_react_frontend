import { Outlet } from "react-router-dom";
import { Box } from "@chakra-ui/react";
import { DashboardUser } from "components/DashboardUser";
import { Header } from "components/Header";
import cls from './styles.module.scss';

export const UsersLayouts = () => {
       return <Box display='flex'>
        <DashboardUser/>
       <Box width='100%'>
        <Header/>
        <Outlet className={cls.outlet} />
       </Box>

     </Box>
}
