import { Outlet } from "react-router-dom";
import { Dashboard } from "components/Dashboard";
import { Box } from "@chakra-ui/react";
import cls from './styles.module.scss'
export const MainLayout = () => {

  return <>
  <Box display='flex'>
    <Dashboard/>
    <Box width='100%'>
    <Outlet className={cls.outlet} />  
    </Box>
  </Box>
  </>;
};
