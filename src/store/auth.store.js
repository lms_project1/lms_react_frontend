import { makeAutoObservable } from "mobx"
import { makePersistable } from "mobx-persist-store";


class AuthStore {

       isAuth = false
       isEmail = ''
       isForgetEmail = ''
       isId = []
       userData = {}
       saveTeacherData = []

       constructor() {
              makeAutoObservable(this);
              makePersistable(this, {
                     name: "auth",
                     storage: window.localStorage,
                     properties: ['isAuth', 'isEmail', 'isForgetEmail', "isId", 'userData', "saveTeacherData",]
              })
       }

       saveUserData(data) {
              this.userData = data
       } 

       saveEmail(data) {
              this.isEmail = data
       }

       saveId(data) {
              this.isId.push(data)
       }

       saveForget(data) {
              this.isForgetEmail = data
       }

       createTeacherData(data) {
              this.saveTeacherData = data
       }

       login() {
              this.isAuth = true
       }

       logout() {
              this.isAuth = false
       }
}

export const authStore = new AuthStore()
